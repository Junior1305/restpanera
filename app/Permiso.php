<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
	public $table = "permiso";

	protected $fillable = [
		'nombre','descripcion','codigo','estado','usuario_registro','fecha_registro','usuario_modificacion','fecha_modificacion'
	];

	public function Perfil_Permisos()
	{
		return $this->hasMany('App\Perfil_Permiso','id','permiso_id');
	}
}
