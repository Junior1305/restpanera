<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
	public $table = "perfil";

	protected $fillable = [
		'nombre','descripcion','estado','usuario_registro','fecha_registro','usuario_modificacion','fecha_modificacion'
	];

	public function Usuario_Perfiles()
	{
		return $this->hasMany('App\Usuario_Perfil','id','perfil_id');
	}

	public function Perfil_Permisos()
	{
		return $this->hasMany('App\Perfil_Permiso','id','perfil_id');
	}
	
}
