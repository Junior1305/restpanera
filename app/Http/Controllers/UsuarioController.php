<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Usuario;
use Validator; 

class UsuarioController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$usuarios = Usuario::all();


		return $this->sendResponse($usuarios->toArray(), 'Usuarios enviados exitosamente.');
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = $request->all();


    	$validator = Validator::make($input, [
    		'usuario' => 'required',
    		'contrasena' => 'required',
    		'nombre' => 'required',
    		'apellido' => 'required'
    	]);


    	if($validator->fails()){
    		return $this->sendError('Validacion Error.', $validator->errors());       
    	}


    	$usuario = Usuario::create($input);


    	return $this->sendResponse($usuario->toArray(), 'Usuario creado exitosamente.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$usuario = Usuario::find($id);


    	if (is_null($usuario)) {
    		return $this->sendError('Usuario no encontrado.');
    	}


    	return $this->sendResponse($usuario->toArray(), 'Usuario enviado exitosamente.');
    }

    public function login(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);// -- Request with JSON pa obtener sus valores 

    	$usuario = Usuario::where('usuario',$input['usuario'])->where('contrasena',$input['contrasena'])->get();

    	if (count($usuario)==0) {
    		return $this->sendError($usuario->toArray(),'Usuario no encontrado.');
    	}
    	
    	return $this->sendResponse($usuario->toArray(), 'Usuario logueado exitosamente.');
    	
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usuario $usuario)
    {
    	$input = $request->all();


    	$validator = Validator::make($input, [
    		'usuario' => 'required',
    		'contrasena' => 'required',
    		'nombre' => 'required',
    		'apellido' => 'required'
    	]);


    	if($validator->fails()){
    		return $this->sendError('Validacion Error.', $validator->errors());       
    	}


    	$usuario->usuario = $input['usuario'];
    	$usuario->contrasena = $input['contrasena'];
    	$usuario->nombre = $input['nombre'];
    	$usuario->apellido = $input['apellido'];
    	$usuario->save();


    	return $this->sendResponse($usuario->toArray(), 'Usuario actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuario $usuario)
    {
    	$usuario->delete();


    	return $this->sendResponse($usuario->toArray(), 'Usuario eliminado exitosamente.');
    }
}
