<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario_Perfil extends Model
{	
	public $table = "usuario_perfil";
	
	protected $fillable = [
		'usuario_id','perfil_id','estado','usuario_registro','fecha_registro','usuario_modificacion','fecha_modificacion'
	];


}
