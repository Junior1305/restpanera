<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
	public $table = "usuario";
	
	protected $fillable = [
		'usuario','contrasena','nombre','apellido','estado','usuario_registro','fecha_registro','usuario_modificacion','fecha_modificacion'
	];

	public function Usuario_Perfiles()
	{
		return $this->hasMany('App\Usuario_Perfil','id','usuario_id');
	}

}
