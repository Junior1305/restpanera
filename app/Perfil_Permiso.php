<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil_Permiso extends Model
{
	public $table = "perfil_permiso";
	
	protected $fillable = [
		'perfil_id','permiso_id','estado','usuario_registro','fecha_registro','usuario_modificacion','fecha_modificacion'
	];

	public function permiso()
	{
		return $this->belongsTo('App\Permiso', 'foreign_key');
	}

	public function perfil()
	{
		return $this->belongsTo('App\Perfil', 'foreign_key');
	}
}
